document.addEventListener("DOMContentLoaded", function () {
    form = document.getElementById("bool");
    thankYouMessage = document.getElementById("thankYouMessage");
    let message = "Thank you for your feedback!"


    form.addEventListener("submit", function (event) {
        event.preventDefault();
        form.style.display = "none";
        thankYouMessage.innerHTML = message;
        thankYouMessage.style.display = "block";
    });
  });