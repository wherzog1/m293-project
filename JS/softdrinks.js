//Softdrinks data

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("../JsonFiles/softdrinks.json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

readTextFile("../JsonFiles/softdrinks.json", function(text)
{
    let softdrinks = JSON.parse(text);
    softdrinks.forEach(element => {
    document.getElementById("softdrinks").innerHTML += 
    `<div class="item">
        <img src="${element.imageUrl}">
        <h2 class="caption">${element.name}</h2>
        <div class="pricetag">
            <select>
                <option>50cl</option>
                <option>100cl</option>
            </select> ${element.prize} <img class="cart" src="../images/shoppingcart.png" alt="Warenkorb" onclick="jsCounter()">
        </div>
    </div>`;
    }); 
})