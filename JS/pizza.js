//Pizza Data

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("../JsonFiles/pizzas.json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

readTextFile("../JsonFiles/pizzas.json", function(text)
{
    let pizzas = JSON.parse(text);
    pizzas.forEach(element => {
        let ingredients = "";
        element.ingredients.forEach(element => {
            ingredients += element;
            ingredients += " , ";
        })
    document.getElementById("pizzas").innerHTML += 
    `<div class="item">
        <div>
            <img src="${element.imageUrl}">
            <div class="inline">
                <h2>${element.name}</h2>
                <h2>${element.prize}</h2><img class="cart" src="../Images/shoppingcart.png" onclick="jsCounter()">
            </div>
        </div>
        <div class="ingredients">${ingredients.substring(0,ingredients.length-3)}</div>
    </div>`;
    }); 
})