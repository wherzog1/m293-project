//Salad Data

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("../Jsonfiles/salads.json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

readTextFile("../JsonFiles/salads.json", function(text)
{
    let salads = JSON.parse(text);
    salads.forEach(element => {
        let ingredients = "";
        element.ingredients.forEach(element => {
            ingredients += element;
            ingredients += " , ";
        })
    document.getElementById("salads").innerHTML += 
    `<div class="item">
    <img src="${element.imageUrl}">
    <h2 class="caption">${element.name}</h2>
    <p class="ingredients">${ingredients.substring(0,ingredients.length-3)}</p>
    <div class="pricetag">
        <select id="dressing green" name="dressing_green">
            <option value="italian_dressing">Italian dressing</option>
            <option value="french_dressing">French dressing</option>
            <option value="no_dressing">No dressing</option>
        </select> ${element.prize} <img class="cart" src="../images/shoppingcart.png" onclick="jsCounter()">
    </div>
    </div>`
    }); 
})